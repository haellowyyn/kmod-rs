// Wrapper header file for bindgen that includes all required
// linux headers.

#include <linux/gfp.h>
#include <linux/printk.h>
#include <linux/slab.h>

const gfp_t EX_GFP_KERNEL = GFP_KERNEL;
